# intrinio

[![CI Status](https://img.shields.io/travis/zachwick/intrinio.svg?style=flat)](https://travis-ci.org/zachwick/intrinio)
[![Version](https://img.shields.io/cocoapods/v/intrinio.svg?style=flat)](https://cocoapods.org/pods/intrinio)
[![License](https://img.shields.io/cocoapods/l/intrinio.svg?style=flat)](https://cocoapods.org/pods/intrinio)
[![Platform](https://img.shields.io/cocoapods/p/intrinio.svg?style=flat)](https://cocoapods.org/pods/intrinio)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

intrinio is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'intrinio'
```

## Author

zachwick, zach@zachwick.com

## License

intrinio is available under the MIT license. See the LICENSE file for more info.
